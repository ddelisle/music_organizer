# encoding: utf-8

import os, sys
import re
import subprocess
#import checkDirs
from mutagen import id3
from mutagen.flac import FLAC, Picture

# --- Configs --- #

rootPath = os.path.normpath("C:/Users/Rex/Downloads/~musicDLtest")
editorPath = os.path.normpath("C:/Program Files/Sublime Text 3/subl.exe")
logPath = os.path.normpath("C:/prog/python/rename/logs/logs_tags.txt")

exts = ["mp3", "flac"]
tagDict = {"Title":    ['TIT2', "title"], 
           "Artist":   ["TPE1", "artist"], 
           "AArtist":  ["TPE2", "albumartist"],
           "Album":    ["TALB", "album"], 
           "TrackNum": ["TRCK", "tracknumber"],
           "Disc":     ["TPOS", "discnumber"],
           "Date":     ["TDRC", "date"], 
           "Genre":    ["TCON", "genre"]
           }
invalidChars = ['<', '>', ':', '\"', '/', '\\', '|', '?', '*']

# --- End Configs --- #

def GetAlbumTrackPaths(rootDir):
    """Makes and returns a list of lists. Each list is an "album" containing a
    list of song paths within that album. The albums are found within an inputted
    root directory.

    Args:
        -rootDir: path to the root (i.e. unsorted downloads) directory
    Returns:
        -albumTrackPaths: list of albums, each containg a list of song paths
    """
    albumTrackPaths = []
    srcDirs = [os.path.join(rootDir, alb) for alb in os.listdir(rootDir)]
    albPths = [albPath for albPath in srcDirs if os.path.isdir(albPath)]

    albumTrackPaths = [[os.path.join(albPth, f) for f in os.listdir(albPth)
                      if f.split(".")[-1].lower() in exts] for albPth in albPths]

    return albumTrackPaths

def UpdateID3(albumTrackPaths):
    """Updates any mp3 songs to have the latest ID3 tag version, currently v2.4. 
    This is done to prevent conflicts with older ID3 versions. An update is
    done easily with a load and save.

    Args:
        -albumTrackPaths: list of albums, each containg a list of song paths
    """
    mp3Count = 0
    for album in albumTrackpaths:
        for songPath in album:
            if songPath.split(".")[-1].lower() == "mp3":
                audio = id3.ID3(songPath)
                audio.save()
                mp3Count += 1

    if mp3Count:
        print "\n%d mp3 files updated to latest: ID3 v2.4" %(mp3Count)

def ClearTags(albumTrackPaths):
    """Takes in a list of audio files, and for each one, deletes any metadata
    or tags that aren't part of a wanted (see tagDict) list. Any tags that are
    removed are written a logfile.

    Args:
        -albumTrackPaths: list of albums, each containg a list of song paths
    Returns:
        -logs: list of strings containing modified song tag information
    """
    logs = [ "\n\t<Removing Unwanted Tags:>"]
    numRemoved = 0

    for album in albumTrackPaths:
        logs.append("\n\t\tAlbum: %s" %(album[0].split("\\")[-2]))
        for songPath in album:
            songName = songPath.split("\\")[-1]

            '''MP3's and FLACs have different tags, so the valid tags are set 
            here. For MP3s, covers / art are in the ID3 frames. For FLACs, a
            separate function must be used to remove any covers / art.'''
            if songPath.split(".")[-1].lower() == "mp3":
                validTags = [tagDict[tag][0] for tag in tagDict]
                audio = id3.ID3(songPath)
            else:
                validTags = [tagDict[tag][1] for tag in tagDict]
                audio = FLAC(songPath)
                audio.clear_pictures()
                audio.save()

            #Remove existing frames (tags) that aren't wanted (valid) ones
            for frame in audio:
                if frame not in validTags:
                    del audio[frame]
                    logs.append("\t\t\t<%s>: %s" %(frame, songName))
                    numRemoved += 1
                audio.save()

    print "%d tags removed. Info in logfile" %(numRemoved)
    return logs

def GetTag(songPath, tagType):
    """Gets the text of a metadata tag for a specified song file.

    Args:
        -songPath: path to the song file
        -tagType: type of tag (e.g. album, artist)
    Returns:
        -tag: string, the text of the tag
    """
    relPath = "\\".join(songPath.split("\\")[-3:])
    isMP3 = songPath.split(".")[-1].lower() == "mp3"
    isFLAC = songPath.split(".")[-1].lower() == "flac"
    tagName = tagDict[tagType][0] if isMP3 else tagDict[tagType][1]

    tag = False
    if isMP3:                               #For MP3 files
        audio = id3.ID3(songPath) 
        if tagName in audio:
            tag = audio[tagName].text[0]
        
    elif isFLAC:                            #For FLAC files
        audio = FLAC(songPath)
        if tagName in audio:
            tag = audio[tagName][0]
        
    return tag

def SetTag(songPath, tagType, tagText, logs):
    """Sets a metadata tag to a song file, either being .mp3 or .flac extension.
    
    Args:
        -songPath: path to the song file
        -tagType: type of tag (e.g. album, artist) 
        -tagText: the text to set as the tag
        -logs: list containing info about modified songs
    Returns:
        -logs: list containing info about modified songs
    """
    songName = songPath.split("\\")[-1]
    isMP3 = songPath.split(".")[-1].lower() == "mp3"
    isFLAC = songPath.split(".")[-1].lower() == "flac"

    if (not isMP3 and not isFLAC):
        print "Error: cannot assign \"%s\" tag to %s" %(tagName, relPath)

    tagName = tagDict[tagType][0] if isMP3 else tagDict[tagType][1]

    '''Mutagen id3 uses separate function names for adding tag elements. Normally 
    this would require a big if/elif block, but the function names are the same
    as the id3 tag identifiers (e.g. to add artist, audio.add(id3.TPE1(..)))
    so we can use getattr() to dynamically call function based on a string.
    '''
    if isMP3:
        audio = id3.ID3(songPath)
        audio.add(getattr(id3, "%s" %(tagName))(text=u'%s' %(tagText)))
        audio.save()

    #FLAC tags are much more simple and thus can be set easily
    else:
        audio = FLAC(songPath)
        audio[tagName] = u'%s' %(tagText)
        
    audio.save()
    logs.append("\t\t\t%s >> %s" %(tagText, songName))

    return logs

def AdjustAlbumArtist(albumTrackPaths):
    """Modifies the "Album Arist" property for a list of album tracks. If for
    all songs in the album, the "artist" tag is the same across them, then the
    album artist can simply be set as the artist. If the artist tag is not 
    consistent, a message is outputted to the terminal.

    Args:
        -albumTrackPaths: list of lists, each lists having a list of song paths
    Returns:
        -logs: list of strings containing modified song tag information
    """
    logs = ["\n\tAdjusting Tag: <Album Artist>"]
    numMods = 0

    for album in albumTrackPaths:

        #If the entire album's "artist" tags are the same, set as album artist
        artistTags = [GetTag(songPath, "Artist") for songPath in album]
        if len(set(artistTags)) == 1:
            artistTag = artistTags[0]
            logs.append("\n\t\tAlbum: %s" %(album[0].split("\\")[-2]))
            for songPath in album:
                logs = SetTag(songPath, "AArtist", artistTag, logs)
                numMods += 1
        else:
            print "Can't set Album Artist for: %s" %(album[0].split("\\")[-2])
    
    numSongs = sum([len(album) for album in albumTrackPaths])
    print "%d / %d tracks modified. Info in logfile." %(numMods, numSongs)
    return logs

def AdjustTrackNum(albumTrackPaths):
    """Modifies the "Track Num" ID3 property of a list of album tracks. The track
    number starts at 1 and increments through the album's tracks. Since the track
    number is correlated with the position of the song in the album list, it is
    assumed that the file names are in order prior to calling this function.

    Args:
        -albTrackPaths: list of albums, each containg list of song paths
    Returns:
        -logs: list of strings containing modified song tag information
    """
    logs = ["\n\tAdjusting Tag: <Track Number>"]
    numMods = 0

    for album in albumTrackPaths:
        zMax = max(len(str(len(album))), 2)
        logs.append("\n\t\tAlbum: %s" %(album[0].split("\\")[-2]))

        for i, songPath in enumerate(album):
            tagText = str(i+1).zfill(zMax)
            logs = SetTag(songPath, "TrackNum", tagText, logs)
            numMods += 1
    
    numSongs = sum([len(album) for album in albumTrackPaths])
    print "%d / %d tracks modified. Info in logfile." %(numMods, numSongs)
    return logs

def AdjustDiscNum(albumTrackPaths):
    """Modifies the "Disc Num" tag property of a list of album tracks. By
    default, the "Disc" property should be nothing (empty string). However, if
    the album is found to be multi-disc, the property will be modified to be
    changed to "X/Y", where X is the current disc, and Y is the total number
    of discs. 

    An album is determined to be multi-disc if the file naming convention inside
    the album has tracks in the form "X-XX. <name>.<ext>"

    Args:
        -albumTrackPaths: list of albums, each containg a list of song paths
    Returns:
        -logs: list of strings containing modified song tag information
    """
    logs = ["\n\tAdjusting Tag: <Disc Number>"]
    numMods = 0

    for album in albumTrackPaths:
        logs.append("\n\t\tAlbum: %s" %(album[0].split("\\")[-2]))
        mDisc = all(path.split("\\")[-1][1] == "-" for path in album)

        #If album multi-disc, add tag. Otherwise, remove disc tag
        for songPath in album:  
            discTag = ""
            if mDisc:  
                totDisc = album[-1].split("\\")[-1][0]
                curDisc = songPath.split("\\")[-1][0]
                discTag = "%s/%s" %(curDisc, totDisc)
            logs = SetTag(songPath, "Disc", discTag, logs)
            numMods += 1

            #TODO: delete disc num tag. Only necessary for FLACs. Setting
            #an empty string in id3 (mp3) will delete it
    
    numSongs = sum([len(album) for album in albumTrackPaths])
    print "%d / %d tracks modified. Info in logfile." %(numMods, numSongs)
    return logs

def AdjustCover(albumTrackPaths):
    """Adds a cover file tag to a list of album tracks. Cover files are looked
    for in the same directory as the album tracks, and should be named 
    "cover.jpg" or "cover.jpeg". If no cover is found in the album directory, a 
    message is added to the log messages.

    Args:
        -albumTrackPaths: list of albums, each containg a list of song paths
    Returns:
        -logs: list of strings containing modified song tag information
    """
    logs = ["\n\tAdjusting Tag: <Cover>"]
    logsNoCovers = ["\n\t\t[No Cover Files Found In Albums:]"]
    logsCovers = ["\n\t\t[Cover Files Applied To:]"]
    coverExts = ['jpg', 'jpeg']
    numMods = 0

    for album in albumTrackPaths:
        albPath = album[0].rsplit("\\", 1)[0]
        albumDir = albPath.split("\\")[-1]

        #First check for presence of cover file
        coverFile = None
        for f in os.listdir(albPath):
            if f[:5] == "cover" and f.split(".")[-1].lower() in coverExts:
                coverFile = os.path.join(albPath, f)

        #If a cover file exists, add cover image to each song in album
        if coverFile:
            imgData = open(coverFile, 'rb').read()
            for songPath in album:
                if songPath.split(".")[-1].lower() == "mp3":
                    audio = id3.ID3(songPath)
                    audio.add(id3.APIC(encoding=3, mime=u'image/jpeg',
                        type=3, data=imgData))
                    audio.save()
                else:
                    audio = FLAC(songPath)
                    audio.clear_pictures()
                    imgFLAC = Picture()
                    imgFLAC.type = id3.PictureType.COVER_FRONT
                    imgFLAC.mime = u"image/jpeg"
                    imgFLAC.data = imgData
                    audio.add_picture(imgFLAC)
                    audio.save()
                numMods += 1

            logsCovers.append("\t\t\t%d songs in: /%s/" %(len(album), albumDir))
        else:
            logsNoCovers.append("\t\t\t%s" %(albumDir))

    logs.extend(logsNoCovers)
    logs.extend(logsCovers)
    numSongs = sum([len(album) for album in albumTrackPaths])
    print "%d / %d tracks modified. Info in logfile." %(numMods, numSongs)
    return logs

def AdjustFileName(albumTrackPaths):
    """Changes the filename of songs inside a list of albums. The title is
    found from text in the "title" tag of the song. Songs are named according
    to the following convention:

    Non-Multi Disc: 01. songname.mp3
    Multi-Disc:     1-01. songname.mp3

    Args:
        -albumTrackPaths: list of albums, each containg a list of song paths
    Returns:
        -logs: list of strings containing modified song tag information
    """
    logs = ["\n\t<Adjusting File Names:>"]
    numChanged = 0

    for album in albumTrackPaths:
        logs.append("\n\t\t%s" %(album[0].rsplit("\\", 1)[0]))

        #Determine if album is multi-disc, and if so, get relevant info
        multiDisc = False
        isFLAC = 1 if (album[-1].split(".")[-1].lower() == "flac") else 0
        audio = FLAC(album[-1]) if isFLAC else id3.ID3(album[-1])
        if tagDict["Disc"][isFLAC] in audio:
            numDiscs = str(audio[tagDict["Disc"][isFLAC]])[-1]
            if numDiscs.isdigit() and int(numDiscs) >= 2:
                multiDisc = True
                numDiscs = int(numDiscs)
                discTrackCt = [1 for d in range(numDiscs)]

        for i, songPath in enumerate(album):

            #Set base file name as song's title, replacing any invalid chars
            if GetTag(songPath, "Title"):
                fTitle = GetTag(songPath, "Title")
                for c in invalidChars:
                    fTitle = fTitle.replace(c, "_")
                baseFN = "%s.%s" %(fTitle, songPath.split(".")[-1])

                #Keep track of disc # and track #, resetting to 1 on new discs
                if multiDisc:
                    curDisc = int(GetTag(songPath, "Disc")[0])
                    curTrack = discTrackCt[curDisc-1]
                    discTrackCt[curDisc-1] += 1
                    newFN = "%d-%s. %s" %(curDisc, str(curTrack).zfill(2), baseFN)
                elif not multiDisc:
                    newFN = "%s. %s" %(str(i+1).zfill(2), baseFN)

                #Rename file if the created file name doesn't match the original
                newSongPath = os.path.join(album[0].rsplit("\\", 1)[0], newFN)
                if newSongPath != songPath:
                    os.rename(songPath, newSongPath)
                    logs.append("\t\t\t%s < %s" %(newFN, songPath.split("\\")[-1]))
                    numChanged += 1
                else:
                    logs.append("\t\t\t[OK] %s" %(newFN))
            else:
                logs.append("\t\t\tNo title tag: %s" %(songPath))

    print "%d filenames changed. Info in logfile." %(numChanged)
    return logs

def AdjustDirName(albumTrackPaths):
    """Renames the containing folder of a list of inputted albums to be of the
    following format:

    (<Year>) <Album>    e.g.: (1973) The Dark Side Of The Moon        

    In order for the directory re-naming to happen, both the "year" as well 
    as the "album" tags must be present and identical for each of the songs
    within the album. For id3 (mp3) tags, the date is stored as a special
    object, so we must convert any non-False elements to strings first.

    Args:
        -albumTrackPaths: list of albums, each containg a list of song paths
    Returns:
        -logs: list of strings containing modified song tag information
    """
    logs = ["\n\tAdjusting Album Directory Paths"]
    logsNoDir = ["\n\t\t[Could not rename directories:]"]
    logsDir = ["\n\t\t[Directories Renamed:]"]
    numDirChange = 0
    numFileMoved = 0

    for album in albumTrackPaths:
        #First make sets for both date and album tags
        dtTemp = {GetTag(songPath, "Date") for songPath in album}
        dateTags = {"%s" %(tag) if tag != False else tag for tag in dtTemp}
        albumTags = {GetTag(songPath, "Album") for songPath in album}

        #Determine truth values for each condition
        hasDates = False not in dateTags
        hasAlbums = False not in albumTags
        oneDate = len(dateTags) == 1
        oneAlbum = len(albumTags) == 1
        dateIsForm = True
        if hasDates and oneDate:
            dateIsForm = re.search(r'^\d{4}$', list(dateTags)[0])

        #If all conditions met, rename album directory
        if all([hasDates, hasAlbums, oneDate, oneAlbum, dateIsForm]):
            dateTag = list(dateTags)[0]
            albumTag = list(albumTags)[0]
            albPathOld = album[0].rsplit("\\", 1)[0]
            albFiles = os.listdir(albPathOld)
            albDirNew = "(%s) %s" %(dateTag, albumTag)
            albPathNew = os.path.join(rootPath, albDirNew)

            #Rename directory if the current one isn't proper form
            if albPathOld != albPathNew:
                #os.rename(albPathOld, albPathNew)
                logsDir.append("\t\t\t[%d files]: %s << %s" 
                    %(len(albFiles), albDirNew, albPathOld.split("\\")[-1]))
            else:
                logsDir.append("\t\t\t[OK]: %s" %(albDirNew))

        #Otherwise, add reason (why we can't rename directory) to logs
        else:
            logsNoDir.append("\t\t\t%s" %(album[0].split("\\")[-2]))
            if len(dateTags) > 1:
                logsNoDir.append("\t\t\t\t-Multiple date tags found")
            if not hasDates:
                logsNoDir.append("\t\t\t\t-Date tag(s) not found")
            if not dateIsForm:
                logsNoDir.append("\t\t\t\t-Improper date format")
            if len(albumTags) > 1:
                logsNoDir.append("\t\t\t\t-Multiple album tags found")
            if not hasAlbums:
                logsNoDir.append("\t\t\t\t-Album tag(s) not found")

    logs.extend(logsNoDir)
    logs.extend(logsDir)
    return logs

def LogTags(albumTrackPaths):
    """Writes the tag information, for a list of songs, to a logfile. 

    Args: 
        -albumTrackPaths: list of albums, each having a sublist of song paths
    Returns:
        -logs: list of strings containing song tag information
    """
    logs = ["\n\t<Printing Tag Data>"]
    for album in albumTrackPaths:
        logs.append("\n\t\tAlbum: %s\n" %(album[0].split("\\")[-2]))

        for songPath in album:
            isMP3 = songPath.split(".")[-1].lower() == "mp3"
            songName = songPath.split("\\")[-1]
            logs.append("\t\t\t%s:" %(songName))

            '''If mp3, use pprint(), giving a built in easy way to print tag 
            data. The string must be split on newlines before it can be
            formatted in the way we want.'''
            if isMP3:
                audio = id3.ID3(songPath)
                for tag in audio.pprint().split("\n"):
                    logs.append("\t\t\t\t%s: %s" %(tag[:4],tag[5:]))

            #FLAC tags are a simple dictionary that can be iterated through
            else:
                audio = FLAC(songPath)
                for key in audio:
                    logs.append("\t\t\t\t%s: %s" %(key, audio[key][0]))
                if audio.pictures:
                    logs.append("\t\t\t\t%s" %(audio.pictures))

    print "\nSuccessfully logged MP3 tag info."
    return logs

def CheckDepth():
    """Checks the file 'depth' of songs within the list of albums. By convention,
    any music files should be directly inside the album directory, and no
    music files in any depths below that (i.e. in subdirectories). Occassionally,
    downloaded albums will have a multi-disc format, and thus potentially contain
    'disc 1 / disc 2' directories inside.

    This function will print out to the terminal all such album directories that
    fail to meet the aforementioned convention.

    Returns:
        badDepthAlbs: list of album directories containing bad song depth
    """
    #Define proper depth of songs relative to root directory
    depthRoot = len(dDir.split("\\"))
    depthAlb = depthRoot + 1
    depthSong = depthRoot + 2
    badDepthAlbs = set()
    errMsg = "\nWarning: Improper song depth found at the following albums:"

    #Add album paths to set if any songs within are out of depth
    for root, subDirs, files in os.walk(dDir):
        for file in files:
            src = os.path.join(root, file)
            badDepth = (len(src.split("\\")) != depthSong)
            isSong = (file.split(".")[-1].lower() in exts)
            if badDepth and isSong:
                badDepthAlbs.add(src.split("\\")[depthRoot])

    #Print out albums with bad song depth
    if badDepthAlbs:
        print errMsg
        for alb in badDepthAlbs:
            print "\t" + alb
        print ""

    return badDepthAlbs

def ChangeDepth(badDepthAlbs):
    """CURRENTLY UNFINISHED

    Moves, renames, and adjusts relevant ID3 tag properties on songs that
    are out of depth. This function is called if any albums contain songs out of
    depth, typically in the case that it's multi-disc.

    Args:
        -badDepthAlbs: list of album directories containing bad depth songs
    """


    albPaths = [os.path.join(dDir, alb) for alb in badDepthAlbs]
    for albPath in albPaths:
        songDirPaths = set()
        for root, subDirs, files in os.walk(albPath, topdown=True):
            for file in files:
                src = os.path.join(root, file)
                srcDir = "\\".join(src.split("\\")[:-1])
                if file.split(".")[-1].lower() in exts:
                    songDirPaths.add(srcDir)

        songDirs = list(songDirPaths)
        songDirs.sort()
        numDiscs = str(len(songDirs))

        for i, songDir in enumerate(songDirs):
            curDisc = str(i+1)
            files = os.listdir(songDir)
            songs = [s for s in files if s.split(".")[-1].lower() in exts]
            for song in songs:
                songPath = os.path.join(albPath, songDir, song)
                discTagInf = "%s/%s" %(curDisc, numDiscs)
                audio = id3.ID3(songPath)
                #if 'TPOS' not in audio:
                #    audio.add(id3.TPOS(encoding=3, text=u'%s'%(discTagInf)))

def CheckFiles(dDir):
    """
    Checks album directories for unwanted files - anything either not a music
    or picture (.jpg) file. Any unwanted files are deleted

    Args:
        dDir: path to the downloads directory
    """
    okExts = ["mp3", "flac", "jpg", "jpeg"]
    uwFiles = []

    #Any file that is not in extension list is added to list (as full path)
    for root, subDirs, files in os.walk(dDir):
        for file in files:
            src = os.path.join(root, file)
            if src.split(".")[-1].lower() not in okExts:
                uwFiles.append(src)

    #Print, log, and remove unwanted files
    if uwFiles:
        log.write("\n\nThe following files were removed:\n")
        print "\n%d unwanted files deleted. See log for details\n" %(len(uwFiles))
        for file in uwFiles:
            log.write("\n\t%s" %(file))
            os.remove(file)

def TagModFunctionSelect():
    """
    """
    albTracks = GetAlbumTrackPaths(rootPath)

    logs = ["Adjusting files in %s" %(rootPath)]

    funcDict = {"x": "Update / Clear Tags", "g": "Log Track Info",
                "f": "Filename", "1": "Album Artist", "2": "Track Number", 
                "3": "Disc Number", "4": "Cover", "d": "Directory Rename"}

    msgSel = "\n<<Select modification function:>>"
    for k in sorted(funcDict):
        msgSel += "\n  (%s): %s" %(k, funcDict[k])
    msgSel += "\n"

    while 1:
        funcSel = raw_input(msgSel)

        if funcSel in funcDict and funcSel != "g":
            print "\nAdjusting Tag Property: <%s>" %(funcDict[funcSel])

        if funcSel == "1":
            logs.extend(AdjustAlbumArtist(albTracks))
        elif funcSel == "2": 
            logs.extend(AdjustTrackNum(albTracks))
        elif funcSel == "3":
            logs.extend(AdjustDiscNum(albTracks))
        elif funcSel == "4":
            logs.extend(AdjustCover(albTracks))
        elif funcSel == "f":
            logs.extend(AdjustFileName(albTracks))
        elif funcSel == "d":
            logs.extend(AdjustDirName(albTracks))
        elif funcSel == "g":
            logs.extend(LogTags(albTracks))
        elif funcSel == "x": 
            logs.extend(ClearTags(albTracks))
        else:
            return logs


if __name__ == "__main__":

    #CheckFiles(dDir)
    #badDepthAlbs = CheckDepth()
    #if badDepthAlbs:
        #ChangeDepth(badDepthAlbs)
    
    print "\nModifying files in: %s" %(rootPath)
    logMessages = TagModFunctionSelect()

    #Open log file, write to it, and close
    log = open(logPath, "w")
    for line in logMessages:
        log.write("%s\n" %(line))
    log.close()

    #Open log file in editor, then program ends
    subprocess.call([editorPath, logPath])


