import sys
import os
import subprocess
import shutil
import time
import requests
import checkDirs
from requests.packages import urllib3
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from bs4 import BeautifulSoup
from PIL import Image

"""Searches, previews, and saves album covers (if so desired) from discogs.com
for every album directory in a list of directories. This list is generated from
checkDirs.py, which will find the directories that presently do not 
have a cover.
"""

#Location of default file viewing software
imageSoftwarePath = "C:\\Windows\\System32\\mspaint.exe"
imageSoftware = "mspaint.exe"
sys.path.append(imageSoftwarePath)

#Length of time to display image preview on screen
previewTime = 2

#Suppress insecure request warning from discogs.com
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

#URL to discogs homepage
http = urllib3.PoolManager()
discogsURL = "http://www.discogs.com"

#Various format searches to try
searchParamURLs = ["&format_exact=CD&type=all",
                   "&format_exact=Album&type=all",
                   "&type=all"]

log = open("logs\\coverNotes.txt", "w")


def GetAlbumPage(album, artist=""):
    """Finds and returns an album page on discogs

    Args:
        album: name of album
        artist: artist name
    Returns:
        0: could not find the album
        albumURL: the URL to the album page on discogs
    """
    albumSearch = album.replace(" ", "+")
    if (artist):
        albumSearch += "+" + artist.replace(" ", "+")
    albumURL = ""
    for searchParam in searchParamURLs:
        searchURL = discogsURL + "/?q=" + albumSearch + searchParam
        searchRequest = http.request('GET', searchURL)
        soup = BeautifulSoup(searchRequest.data, "html.parser")
        searchResults = soup.find_all("h4")
        for result in searchResults:
            searchTitle = result.find_all("a")[0]['title']
            if (album.lower() in searchTitle.lower()):
                link = result.find_all("a")[0]['href']
                if '/master/' in link:
                    albumURL = discogsURL + link
                    return albumURL
                elif '/release/' in link:
                    albumURL = discogsURL + link
                    return albumURL

    return albumURL
    
def GetAlbumSongs(albumURL):
    """Gets a tracklist of songs from a given album page on discogs

    Args:
        albumURL: the URL to the album page on discogs
    Returns:
        trackList: list of tracks on the album
    """
    albumRequest = http.request('GET', albumURL)
    soup = BeautifulSoup(albumRequest.data, "html.parser")
    pageTrackList = soup.find_all("table", class_="playlist")
    trackList = []
    songs = pageTrackList[0].find_all("span", class_="tracklist_track_title")
    for i in range(len(songs)):
        trackList.append(songs[i].text)

    return trackList

def PreviewImage(rawImage):
    """Creates a temporary file, then runs a subprocess that opens the 
    image in an image viewing software. The image will be displayed for
    a defined wait time, then the subprocess is kill, and the temporary
    file is deleted.

    Args:
        rawImage: the raw image data to display
    """
    with open('temp.jpg', 'wb') as tempFile:
        shutil.copyfileobj(rawImage, tempFile)
    tempFile.close()
    processImageView = subprocess.Popen([imageSoftware, 'temp.jpg'])
    time.sleep(previewTime)
    processImageView.kill()
    os.remove('temp.jpg')

def GetAlbumCover(albumURL, albumPath=""):
    """Finds an album's image page on www.discogs.com, goes through each
    image, and prompts the user to select which one to choose as the
    cover.

    Args:
        albumURL: URL to the album's page on discogs
        albumPath: path to the location to place album cover
    """

    #Retrieve the link to the images page for the album
    albumRequest = http.request('GET', albumURL)
    soupAlbum = BeautifulSoup(albumRequest.data, "html.parser")
    imagePageCont = soupAlbum.find_all("div", class_="image_gallery")[0]
    imagePageLink = imagePageCont.find_all("a")[0]['href']
    imagePageURL = discogsURL + imagePageLink

    #Get all HTML with a "span" containing an image
    imagePageRequest = http.request('GET', imagePageURL)
    soupImage = BeautifulSoup(imagePageRequest.data, "html.parser")
    imageSpans = soupImage.find_all("span", class_="thumbnail_link")

    #Return if no images found
    if not imageSpans:
        print "Could not find any images on album's image page"
        return

    artist = albumPath.split("\\")[2]
    alb = albumPath.split("\\")[-1][7:]
    print "\nSearching album covers for: %s - %s\n" %(artist, alb)

    #Show found images one by one. At each, prompt user if they want
    #to select it. If user selects, copy the image to the album directory
    for i in range(len(imageSpans)):
        imageURL = imageSpans[i].find_all("img")[0]['src']
        imageObject = requests.get(imageURL, stream=True)
        imageRaw = imageObject.raw
        PreviewImage(imageRaw)
        
        selPrefix = "Image %s of %s: " %(i+1, len(imageSpans))
        selPrompt = "Select this image? 0-No / 1-Yes / 9-skip all: "
        select = int(raw_input(selPrefix + selPrompt))
        if (select == 1):
            imageFormat = ".%s" %(imageURL.rsplit(".", 1)[1])
            imageFileName = "cover%s" %(imageFormat)
            imagePath = os.path.join(albumPath, imageFileName)
            imageObj = requests.get(imageURL, stream=True)

            with open(imagePath, 'wb') as coverFile:
                shutil.copyfileobj(imageObj.raw, coverFile)
            coverFile.close()
            print "\nSuccess! Cover found and copied to \n\t%s\n" %(imagePath)
            return
        elif (select == 9):
            break
        

    log.write("\n%s" %(albumPath))
    return

def GetCovers(albumPaths):
    for albumPath in albumPaths:
        artist = albumPath.split("\\")[2]
        if (artist.endswith(", The")):
            artist = "The " + artist[:-5]
        album = albumPath.rsplit("\\", 1)[1][7:]
        albumURL = GetAlbumPage(album, artist)
        if (albumURL):
            GetAlbumCover(albumURL, albumPath)


if __name__ == "__main__":
    albumsNoCover = checkDirs.GetAlbumsNoCover()
    if (albumsNoCover):
        GetCovers(albumsNoCover)